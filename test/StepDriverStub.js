// @ts-check

const Device = require('./Device');

class StepDriverStub extends Device {
  /**
   * @param {string} name
   */
  constructor(name) {
    super(name);
    this.addProperty('Acquisition', { data: {
      homing: false, homeSwitch: true,
      ccwSwitch: true, cwSitch: false,
      positionName: '',
      actualPosition: { value: 0, type: 'Float' },
      actualPosition_units: 'mm', // eslint-disable-line camelcase
      actualPosition_min: { value: 0, type: 'Float' }, // eslint-disable-line camelcase
      actualPosition_max: { value: 100, type: 'Float' }, // eslint-disable-line camelcase
      state: 0 } });
    this.addProperty('Home', { write: true, data: {} });
    this.addProperty('AbsoluteAxisPosition', { write: true, data: {
      position: { value: 0, type: 'Float' }
    } });
    this.addProperty('RelativeAxisPosition', { write: true, data: {
      position: { value: 0, type: 'Float' }
    } });
    this.addProperty('PredefinedPosition', { write: true, data: {
      positionNumber: { value: 0, type: 'Int32' }
    } });
    this.addProperty('Status', { data: {
      offline: false,
      errorMessages: { value: [], type: 'String' },
      lastErrorMessage: '' } });
    this.addProperty('ExpertSetting', { data: {
      axisSpeed: { value: 0, type: 'Float' },
      axisAcceleration: { value: 0, type: 'Float' },
      homePosition: { value: 0, type: 'Float' },
      homingSpeed: { value: 0, type: 'Float' },
      predefinedPositions: { value: [], type: 'Float' },
      predefinedPositionsNames: { value: [], type: 'String' }
    } });
  }
}

// @ts-ignore
if (require.main === module) {
  // @ts-ignore
  const device = new StepDriverStub(process.argv[2] ||
    'test-device.localhost'); /* jshint unused:false */
  console.log('device running', device.name);
}
else {
  module.exports = StepDriverStub;
}
