// @ts-check

const
  { forEach, unset, size, isString, get, throttle, isNil,
    delay } = require('lodash'),
  RDA3 = require('@cern/rda3'),
  debug = require('debug')('test:stub');

/**
 * @typedef {{
 *  name: string,
 *  read?: boolean,
 *  write?: boolean,
 *  listeners?: { [id: number]: RDA3.Server.SubscriptionSource },
 *  data: RDA3.AcquiredData
 * }} Property
 *
 * @typedef {{
 *  read?: boolean,
 *  write?: boolean,
 *  subscribe?: boolean,
 *  data: { [name: string]: boolean|string|{ value: any, type: String|RDA3.Data.NumberType } }
 * }} PropertyDesc
 */

class Device {
  /**
   * @param {string} name the device name
   */
  constructor(name) {
    this.name = name;
    this.srv = new RDA3.Server(name);
    this.count = 0;

    /** @type {{ [name: string]: Property }} */
    this.props = {};

    this.srv.setSubSrcAddedCallback((req) => this.onSubSrcAdded(req));
    this.srv.setSubSrcRemovedCallback((req) => this.onSubSrcRemoved(req));
    this.srv.setSubscribeCallback((req) => this.onSub(req));
    this.srv.setGetCallback((req) => this.onGet(req));
    this.srv.setSetCallback((req) => this.onSet(req));
    this.notifyAll = throttle(this.notifyAll, 0, { leading: false });
    delay(() => this.start(), 0);
  }

  start() {
    debug('starting device');
    this.srv?.start();
  }

  release() {
    debug('stopping device');
    this.srv?.stop();
    this.srv = null;
  }

  /**
   * @param {string} name name of the property
   * @param {PropertyDesc} desc property description
   */
  addProperty(name, desc) {
    /** @type {Property} */
    const prop = {
      name,
      read: desc.read ?? true,
      write: desc.write ?? false,
      data: new RDA3.AcquiredData()
    };
    if (desc.subscribe ?? true) {
      prop.listeners = {};
    }
    prop.data.cycleName = "";
    prop.data.cycleStamp = BigInt(0);
    prop.data.acqStamp = BigInt(0);
    prop.data.data = new RDA3.Data();

    forEach(desc.data, (/** @type {any} */ value, key) => {
      if (isNil(value?.type)) {
        prop.data.data.append(key, value);
      }
      else {
        prop.data.data.append(key, value.value, value.type);
      }
    });
  }

  /** @param {RDA3.Server.SubscriptionSource} req */
  onSubSrcAdded(req) {
    const prop = this.props[req.Property];
    if (!prop) {
      throw new Error("unknown property");
    }
    else if (!prop.listeners) {
      throw new Error("property not subscribable");
    }
    debug('adding sub %i on prop %s', req.Id, req.Property);
    prop.listeners[req.Id] = req;
  }

  /** @param {RDA3.Server.SubscriptionSource} req */
  onSubSrcRemoved(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('removing sub %i', req.Id);
    unset(prop, req.Id);
  }

  /** @param {RDA3.Server.Request} req */
  onSub(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('client get value on prop %s', req.Property);
    return prop.data;
  }

  /** @param {RDA3.Server.Request} req */
  onGet(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('client get value on prop %s', req.Property);
    return prop.data.data;
  }

  /** @param {RDA3.Server.Request} req */
  onSet(req) {
    const prop = this.props[req.Property];
    if (!prop) {
      throw new Error("unknown property");
    }
    if (!prop.write) {
      throw new Error("property not writable");
    }
    debug('client set value on prop %s', req.Property);
    forEach(get(req, [ 'Data', 'entries' ]), (value, key) => {
      prop.data.data.remove(key);
      prop.data.data.append(key, value);
      this.notifyAll(prop);
    });
  }

  /**
   * @param {Property|string} prop
   * @param {string} name
   * @param {any} value
   */
  set(prop, name, value) {
    /** @type {Property|undefined} */
    const property = isString(prop) ? this.props[prop] : prop;
    if (!property) { throw new Error('unknown property'); }

    const type = property.data.data.getDataType(name);
    property.data.data.remove(name);
    property.data.data.append(name, value, type);
    this.notifyAll(property);
  }

  /**
   * @param  {Property|string} prop
   */
  notifyAll(prop) {
    /** @type {Property|undefined} */
    const property = isString(prop) ? this.props[prop] : prop;
    debug('sending %i notifications on %s to %i listeners', property?.name,
      size(property?.listeners));
    forEach(property?.listeners, (l) => l.notify(property.data));
  }
}

module.exports = Device;
