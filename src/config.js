const
  { tryLoadFile } = require('@cern/js-yaml'),
  { defaultTo } = require('lodash');

/** @type {any} */ /* eslint-disable-next-line global-require */
const config = tryLoadFile('/etc/app/config.yml') || require('./config-stub');

config.port = defaultTo(config.port, 8080);
config.basePath = defaultTo(config.basePath, '');

module.exports = /** @type {AppServer.Config} */ (config);
