// @ts-check

const
  { transform } = require('lodash'),
  express = require('express'),
  CMWServer = require('@cern/cmw-express');

class DeviceServer {
  /**
   * @param {string[]} devices
   */
  constructor(devices) {
    const ep = transform(devices, (ret, device) => {
      ret.push({ device, property: 'Acquisition', unsubscribeDelay: 3600 });
      ret.push({ device, property: 'Status', unsubscribeDelay: 3600 });
      ret.push({ device, property: 'Home',
        read: false, write: true, monitor: false });
      ret.push({ device, property: 'Stop',
        read: false, write: true, monitor: false });
      ret.push({ device, property: 'AbsoluteAxisPosition',
        unsubscribeDelay: 3600, write: true });
      ret.push({ device, property: 'RelativeAxisPosition',
        unsubscribeDelay: 3600, write: true });
      ret.push({ device, property: 'PredefinedPosition',
        unsubscribeDelay: 3600, write: true });
      ret.push({ device, property: 'ExpertSetting',
        unsubscribeDelay: 3600, write: true });
    }, /** @type {CMWServer.EPConfig[]} */ ([]));

    this.cmw = new CMWServer({ endpoints: ep });
  }

  /**
   * @param  {express.IRouter} app
   */
  register(app) {
    const router = express.Router();
    this.cmw.register(router);
    app.use('/api', router);
  }

  release() {
    this.cmw.release();
  }
}

module.exports = DeviceServer;
