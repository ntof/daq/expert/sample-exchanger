// @ts-check
const
  Server = require('./Server');

const config = require('./config');

var server = new Server(config);

// @ts-ignore
if (!module.parent) {
  (async function() {
    /* we're called as a main, let's listen */
    await server.listen(() => {
      console.log(`Server listening on port http://localhost:${config.port}`);
    });
  }());

}
else {
  /* export our server */
  module.exports = server;
}
