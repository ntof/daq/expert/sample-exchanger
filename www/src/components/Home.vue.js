// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';

import SampleExchangerController from './SampleExchanger/Controller.vue';

const component = Vue.extend({
  name: 'Home',
  components: { SampleExchangerController },
  computed: {
    .../** @type {{ earName(): string? }} */mapState('route', {
      earName: (r) => r?.query?.area
    }),
    .../** @type {{ areaMap(): { [name: string]: string } }} */mapState([ 'areaMap' ]),
    /** @return {string|null} */
    device() {
      return this.areaMap?.[this.earName];
    }
  }

});
export default component;
