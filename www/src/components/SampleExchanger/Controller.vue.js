// @ts-check

import { isNaN, isNil, toNumber } from 'lodash';
import {
  BaseKeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { AxisState, AxisStateName } from '../../interfaces/SampleExchanger';
import PositionEdit from './PositionEdit.vue';

import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<options, any>> &
 *   V.Instance<ReturnType<BaseKeyboardEventMixin>>
 * } Instance
 */

const options = {
  AxisState
};

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'SampleExchangerController',
  components: { PositionEdit },
  mixins: [ BaseKeyboardEventMixin({ local: false }) ],
  ...options,
  /** @return {{
   *    absTarget: string|null, relTarget: string|null, inEdit: boolean,
   *    predefTarget: number|null, blink: boolean
   *  }}
   */
  data() {
    return {
      absTarget: null, relTarget: null, predefTarget: null,
      inEdit: false, blink: true
    };
  },
  computed: {
    .../** @type {{
      Acquisition(): Iface.Acquisition,
      Status(): Iface.Status,
      ExpertSetting(): Iface.ExpertSetting
    }} */(mapState([ 'Acquisition', 'Status', 'ExpertSetting' ]))
  },
  watch: {
    Acquisition() {
      this.blink = !this.blink;
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => { this.inEdit = true; });
    this.onKey('esc', () => { this.inEdit = false; });
  },
  methods: {
    /** @param {number} state */
    toStateName(state) {
      return AxisStateName[state] ?? "UNKNOWN";
    },
    async runHoming() {
      return this.$store.sources.sampleExchanger.sendHome()
      .catch(logger.error);
    },
    /** @param {number} position */
    async moveTo(position) {
      return this.$store.sources.sampleExchanger.sendAbsolute(toNumber(position))
      .catch(logger.error);
    },
    /** @param {number} position */
    async moveOf(position) {
      return this.$store.sources.sampleExchanger.sendRelative(toNumber(position))
      .catch(logger.error);
    },
    async stop() {
      return this.$store.sources.sampleExchanger.sendStop()
      .catch(logger.error);
    },
    /** @param {number} position */
    async movePredefined(position) {
      if (isNaN(position) || isNil(position) || (position < 0)) {
        return;
      }

      return this.$store.sources.sampleExchanger.sendPredefined(toNumber(position))
      .catch(logger.error);
    }
  }
});
export default component;
