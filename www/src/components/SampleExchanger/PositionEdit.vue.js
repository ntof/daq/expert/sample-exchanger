// @ts-check

import { isEmpty, map } from 'lodash';
import {
  BaseKeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import draggable from 'vuedraggable';

import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component> &
 *   V.Instance<ReturnType<BaseKeyboardEventMixin>>
 * } Instance
 */

const component = Vue.extend({
  name: 'SampleExchangerPositionEdit',
  components: { draggable },
  mixins: [ BaseKeyboardEventMixin({ local: false }) ],
  /** @return {{ positions: { name: string, position: number }[]|null }} */
  data() {
    return { positions: null };
  },
  computed: {
    .../** @type {{ ExpertSetting(): Iface.ExpertSetting, Acquisition(): Iface.Acquisition }} */(mapState(
      [ 'ExpertSetting', 'Acquisition' ]))
  },
  watch: {
    ExpertSetting: {
      handler() { if (isEmpty(this.positions)) { this.update(); } },
      immediate: true
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', (/** @type {KeyboardEvent} */ evt) => {
      evt.preventDefault();
      this.send();
    });
  },
  methods: {
    update() {
      this.positions = map(
        this.ExpertSetting?.predefinedPositionsNames, (name, idx) => {
          return { name,
            position: this.ExpertSetting?.predefinedPositions?.[idx] ?? 0 };
        });
    },
    /** @param {number} index */
    removeAt(index) {
      this.positions?.splice(index, 1);
    },
    add() {
      this.positions?.push({ name: '', position: 0 });
    },
    /** @param {number} index */
    setCurrentPosition(index) {
      const pos = this.positions?.[index];

      if (!pos) { return; }
      pos.position = this.Acquisition?.actualPosition || 0;
    },
    async send() {
      this.$store.sources.sampleExchanger.sendPredefinedConfig(this.positions ?? [])
      .then(
        () => this.$emit('updated'),
        logger.error.bind(logger));
    }
  }
});
export default component;
