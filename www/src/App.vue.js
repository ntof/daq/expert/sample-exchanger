// @ts-check

import { cloneDeep, noop } from 'lodash';
import { TITLE, VERSION } from './Consts';
import Vue from 'vue';
import { mapState } from 'vuex';

const component = Vue.extend({
  name: 'App',
  /**
   * @return {{
   *  TITLE: string,
   *  VERSION: string
   * }}
   */
  data() { return { TITLE, VERSION }; },
  computed: {
    .../** @type {{ earName(): string? }} */mapState('route', {
      earName: (r) => r?.query?.area
    }),
    .../** @type {{ areaMap(): { [name: string]: string } }} */mapState(
      [ 'areaMap' ])
  },
  methods: {
    /** @param {string} name */
    selectEar(name) {
      var query = cloneDeep(this.$route.query);
      query.area = name;
      this.$router.push({ query }).catch(noop);
    }
  }
});

export default component;
