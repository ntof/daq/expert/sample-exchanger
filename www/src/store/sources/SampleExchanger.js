// @ts-check

import { currentUrl, toWs } from '../../utilities';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import d from 'debug';
import { map, noop, toNumber } from 'lodash';

const debug = d('source:se');
const WS_GOING_AWAY = 1001;

/** @param {any} ret */
function cmwCheck(ret) {
  if (ret?.data?.error) { throw new Error(ret?.data?.error); }
  return ret;
}


export default class SampleExchanger {
  /** @param {V.Store<BaseVue.StoreState>} store */
  constructor(store) {
    this.$store = store;
    this.fetchAreaMap();
    /** @type {string|null} */
    this.device = null;
    /** @type {WebSocket[]} */
    this._ws = [];

    /** @type {function|null} */
    this._storeWatch = this.$store.watch(
      (state, getters) => getters?.device,
      (device) => this.updateDevice(device));
  }

  destroy() {
    this._storeWatch?.();
    this._storeWatch = null;
    this.close();
  }

  close() {
    this._ws.forEach((ws) => {
      try {
        ws.onerror = noop;
        ws.close(WS_GOING_AWAY);
      }
      catch (err) {
        console.log(err);
      }
    });
    this._ws = [];
  }

  async fetchAreaMap() {
    try {
      const ret = await axios.get(currentUrl() + '/devices');
      this.$store.commit('update', { areaMap: ret?.data ?? {} });
    }
    catch (err) {
      logger.error(err);
    }
  }

  /**
   * @param  {string|null} device
   */
  updateDevice(device) {
    if (device !== this.device) {
      this.device = device;
      debug('device changed: %s', device);
      this.close();

      if (!this.device) { return; }

      for (const source of [ 'Acquisition', 'PredefinedPosition', 'Status', 'ExpertSetting' ]) {
        const url = `${toWs(currentUrl())}/api/${this.device}/${source}`;
        const ws = new WebSocket(url);
        ws.binaryType = 'arraybuffer';
        ws.onmessage = (evt) => {
          try {
            const data = JSON.parse(evt?.data);
            this.$store.commit('update', { [source]: data?.data?.entries });
          }
          catch (err) {
            console.warn(err);
          }
        };
        ws.onerror = () => logger.error(`failed to connect ${url}`); // jshint ignore:line
        this._ws.push(ws);
      }
    }
  }

  async sendHome() {
    return axios.put(`${currentUrl()}/api/${this.device}/Home`)
    .then(cmwCheck);
  }

  /** @param {number} position */
  async sendAbsolute(position) {
    return axios.put(`${currentUrl()}/api/${this.device}/AbsoluteAxisPosition`,
      { position: { type: 'Float', value: position } })
    .then(cmwCheck);
  }

  /** @param {number} position */
  async sendRelative(position) {
    return axios.put(`${currentUrl()}/api/${this.device}/RelativeAxisPosition`,
      { position: { type: 'Float', value: position } })
    .then(cmwCheck);
  }

  /** @param {number} position */
  async sendPredefined(position) {
    return axios.put(`${currentUrl()}/api/${this.device}/PredefinedPosition`,
      { positionNumber: { type: 'Int32', value: position } })
    .then(cmwCheck);
  }

  async sendStop() {
    return axios.put(`${currentUrl()}/api/${this.device}/Stop`)
    .then(cmwCheck);
  }

  /**
   * @param  {{ name: string, position: number }[]}  positions
   */
  async sendPredefinedConfig(positions) {
    return axios.put(`${currentUrl()}/api/${this.device}/ExpertSetting`, {
      predefinedPositions: {
        type: 'Float', value: map(positions, (v) => toNumber(v.position)) },
      predefinedPositionsNames: map(positions, 'name')
    })
    .then(cmwCheck);
  }
}
