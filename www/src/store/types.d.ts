
import { StoreSources, StoreState } from '@cern/base-vue';
import { ExtVue } from "../Vue-ext";
import IFace from '../interfaces/types';

declare module "@cern/base-vue" {
  declare interface StoreState {
    areaMap: { [name: string]: string },
    Acquisition: IFace.Acquisition|null,
    ExpertSetting: IFace.ExpertSetting|null,
    Status: IFace.Status|null
  }

  declare interface StoreSources {
    sampleExchanger: import('./sources/SampleExchanger').default
  }
}
declare module "../Vue-ext" {
  declare interface ExtVue<Opts, Refs> {
    $store: V.Store<BaseVue.StoreState>
  }
}
