// @ts-check

import { assign, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  createStore,
  storeOptions } from '@cern/base-vue';

/**
 * @typedef {import('vuex').StoreOptions<any>} StoreOptions
 */

import SampleExchanger from './sources/SampleExchanger';

Vue.use(Vuex);

merge(storeOptions, /** @type {StoreOptions} */ ({
  state: {
    areaMap: {},
    Acquisition: null,
    PredefinedPosition: null,
    Status: null,
    ExpertSetting: null
  },
  mutations: {
    update: assign
  },
  getters: {
    /** @type {(state: BaseVue.StoreState) => string|undefined} */
    device: (state) => state?.areaMap?.[state.route?.query?.area ?? ""]
  }
}));

storeOptions.plugins.push(
  (/** @type {V.Store<BaseVue.StoreState>} */ store) => {
    store.sources = merge(store.sources, {
      sampleExchanger: new SampleExchanger(store)
    });
  });

export default createStore;
