// @ts-check

import { startsWith } from 'lodash';

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return 'a-' + (++id);
}

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}


/**
 * @param {string} url
 * @return {string}
 */
export function toWs(url) {
  if (startsWith(url, 'http://')) {
    return 'ws://' + url.slice(7);
  }
  else if (startsWith(url, 'https://')) {
    return 'wss://' + url.slice(8);
  }
  else {
    return url;
  }
}
