// @ts-check

import { invert } from 'lodash';

/** @enum {number} */
export const AxisState = {
  STEADY: 0,
  ACCELERATING: 1,
  MOVING: 2,
  DECELERATING: 3
};

export const AxisStateName = invert(AxisState);
