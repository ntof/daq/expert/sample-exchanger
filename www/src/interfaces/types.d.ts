// @ts-check

import { AxisState } from './SampleExchanger';

export = Iface
export as namespace Iface

declare namespace Iface {
  /* declare your interfaces here */
  interface ExpertSetting {
    predefinedPositions: number[]
    predefinedPositionsNames: string[]
  }

  interface Acquisition {
    actualPosition: number
    actualPosition_max: number // eslint-disable-line
    actualPosition_min: number // eslint-disable-line
    actualPosition_units: string // eslint-disable-line
    cwSwitch: boolean
    ccwSwitch: boolean
    homeSwitch: boolean
    positionName: string
    positionNumber: number
    state: AxisState
    homing: boolean
  }

  interface Status {
    offline: boolean
    errorMessages: string[]
  }
}
