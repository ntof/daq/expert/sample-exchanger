const
  { karmaConfig } = require('karma-mocha-webpack');

module.exports = function(karma) {
  karma.set(karmaConfig(karma));
};
