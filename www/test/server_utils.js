
const
  { makeDeferred } = require('@cern/prom'),

  Server = require('../../src/Server');

/**
 * @param  {any} env
 */
function createApp(env) {
  var def = makeDeferred();

  env.server = new Server({
    port: 0, basePath: '',
    db: {
      client: 'sqlite3', useNullAsDefault: true,
      connection: { filename: ':memory:' }
    }
  });

  env.server.listen(() => def.resolve(undefined));
  return def.promise;
}

module.exports = { createApp };
